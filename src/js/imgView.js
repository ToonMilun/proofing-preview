define([
], function () {

    class ImgView extends Backbone.View {

        initialize() {
            this.render();
        }

        className() {
            return "gallery__item";
        }

        attributes() {
            return {
                href: this.model.get("url")
            }
        }

        events() {
            return {
                "click .js-gallery-btn" : "onClick"
            }
        }

        render(){
            let url = this.model.get("url");
            let name = this.model.get("name");

            // If the URL contains ".eps", then provide a Shutterstock link instead.
            if (url.match(/[.]eps$/g)) {

                // Get the shutterstock code (if any).
                let ssCode = url.match(/(\d+)[.]eps$/);
                let ssURL = "";
                if (ssCode && ssCode.length > 0) {
                    ssCode = ssCode[1];
                    ssURL = `https://www.shutterstock.com/search/${ssCode}`;
                }

                this.$el.html(`
                <div class="gallery__item-inner" title=".eps files cannot be previewed.">
                    ${ssURL ? `<div class="gallery__item-hyperlink"><a href="${ssURL}" target="_blank">Shutterstock</a></div>` : ""}
                    <div class="gallery__item-img js-gallery-btn"><img class="img img-shutterstock" src="src/assets/shutterstock-icon.png" /></div>
                    <div class="gallery__item-name">${name}</div>
                </div>`);

                return;
            }
            
            this.$el.html(`
                <div class="gallery__item-inner">
                    <div class="gallery__item-img js-gallery-btn"><img class="img" src="${url}" /></div>
                    <div class="gallery__item-name">${name}</div>
                </div>`);

            //<a class="gallery__item-anchor" href="${url}" target="_blank"></a>
        }

        // Open images in a new tab.
        onClick() {
            window.open(this.model.get("url"), '_blank').focus();
        }
    }

    return ImgView;
});
