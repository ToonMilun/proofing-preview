define([
    "page"
], function(Page) {

    /**
     * router.js
     * 
     * Monitors for changes to the URL
     * and serves as an entry point into the page.
     */

    // https://dzone.com/articles/backbone-tutorial-part-7
    
    class MainRouter extends Backbone.Router {

        initialize() {

            Backbone.history.start(); // Tells Backbone to start watching for hashchange events
        }

        /**
         * All Backbone Routes
         */
        routes() {
            return {
                /**
                 * When there is no hash on the url, the home method is called
                 */
                "": "handleRoute",
                "*url": "handleRoute"
            }
        }

        renderGuidePage(pageName){

            // Remove previously rendered page.
            if (this.pageView) {
                this.pageView.remove();
            }

            // Render the corresponding page.
            this.pageView = new PageView({
                model: DD.pages.guides.getPageById(pageName)
            });
            DD.pages.guides.setPageActiveById(pageName);
            $(".main__inner").html(this.pageView.$el);
        }

        // Routes
        // ------------------------------------

        handleRoute(url) {

            $(".js-banner-dir").html(url);

            // If no URL, go inside the "images" folder.
            if (!url) {
                this.navigate("images/");
                this.handleRoute("images/");
                return;
            }
            
            new Page(url || "");
        }
    }

    return MainRouter;
});