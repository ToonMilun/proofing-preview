define([
], function () {

    class DirView extends Backbone.View {

        initialize() {
            this.render();
        }

        className() {
            return "gallery__item";
        }

        events() {
            return {
                "click .js-gallery-btn" : "onClick"
            }
        }

        render(){
            let name = this.model.get("name");

            this.$el.html(`
                <div class="gallery__item-inner">
                    <div class="gallery__item-icon js-gallery-btn">📁</div>
                    <div class="gallery__item-name">${name}</div>
                </div>`);
        }

        onClick() {
            router.navigate(this.model.get("url"));
        }
    }

    return DirView;
});
