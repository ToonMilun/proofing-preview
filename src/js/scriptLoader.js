// For any third party dependencies, like jQuery, place them in the lib folder.

requirejs.config({
    waitSeconds:    30,
    baseUrl: 'src/js',
    paths: {

        libraries:  'lib',

        jquery:     'lib/jquery-3.6.0.min',
        underscore: 'lib/lodash',
        backbone:   'lib/backbone'
    },
    shim: {

        // Backbone
        "backbone": {
  
           // Depends on underscore/lodash and jQuery
           "deps": ["underscore", "jquery"],

           // Exports the global window.Backbone object
           "exports": "Backbone"
        }
    },
    catchError: true
});

// Start loading the main app file. Put all of
// your application logic in there.
requirejs([
    'main'
]);
