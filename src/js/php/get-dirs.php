<?php
    // Get all directories in the current directory.
    $url = $_POST['url'];
    $url = getcwd() . '/../../../' . $url . '*';

    // Note: getcwd() returns "proofing-preview\src\js\php". Deliberately navigate backwards to get to the root (FIXME: HARDCODED).

    $dirs = glob($url, GLOB_ONLYDIR);
    foreach ($dirs as $dir) {

        $values = parse_url($dir);
        $values['sections'] = explode('/', $values['path']);

        $end = end($values['sections']);
        echo $end . ",";
    }
?>