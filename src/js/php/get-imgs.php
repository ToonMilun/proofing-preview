<?php
    // Get all directories in the current directory.
    $url = $_POST['url'];
    $url = getcwd() . '/../../../' . $url . '*.{[jJ][pP][gG],[pP][nN][gG],[gG][iI][fF],[eE][pP][sS],[sS][vV][gG]}';

    // Note: getcwd() returns "proofing-preview\src\js\php". Deliberately navigate backwards to get to the root (FIXME: HARDCODED).

    $imgs = glob($url, GLOB_BRACE);
    foreach ($imgs as $img) {

        $values = parse_url($img);
        $values['sections'] = explode('/', $values['path']);

        $end = end($values['sections']);
        echo $end . ",";
    }
?>