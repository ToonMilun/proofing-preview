define([
    "backbone",
    'lib/handlebars'
], function (Backbone, Handlebars) {

    // Global variables
    // ----------------
    // These are referred to constantly.
    window.Backbone = Backbone;
    window.Handlebars = Handlebars;

    class Main extends Backbone.Model {

        async initialize() {

            require(['router'], (Router) => {
                window.router = new Router();

                // Allow the user to return to the standard FTP browser view.
                $(".js-banner-btn").on("click", () => {
                    window.location.href = Backbone.history.getFragment();
                });
            });
        }
    }

    new Main();
});
