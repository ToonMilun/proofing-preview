define([
    "dirView",
    "imgView"
], function (DirView, ImgView) {

    /**
     * Renders all directories and images at a specific URL.
     */

    class PageModel extends Backbone.Model {

        initialize() {
            console.log(`-- Setting up page for: ${this.get("url") || "./"} --`);
        }
    }

    class PageView extends Backbone.View {

        initialize() {
            this.clear();

            this.getDirs();
            this.getImgs();
        }

        events() {
            return {
                //"click .js-banner-btn" : "onBannerBtnClick"
            }
        }

        // Removes any previously rendered items.
        clear() {
            $("#gallery-imgs, #gallery-dirs").html("");
        }

        // https://code.tutsplus.com/tutorials/how-to-call-a-php-function-from-javascript--cms-36508
        getDirs() {

            $.ajax({
                method: "POST",
                url: "src/js/php/get-dirs.php",
                data: {url: this.model.get("url") },
                success: function (data) {

                    let dirs = data.split(",");
                    dirs = dirs.filter(e => e); // Remove any empty elements (if they exist).
                    
                    console.log("Found dirs:", dirs);

                    dirs.forEach((dir) => {
                        $("#gallery-dirs").append(new DirView({model: new Backbone.Model({
                            url: Backbone.history.getFragment() + dir + "/",
                            name: dir
                        })}).$el);
                    });
                }
            });
        }

        getImgs() {

            $.ajax({
                method: "POST",
                url: "src/js/php/get-imgs.php",
                data: {url: this.model.get("url") },
                success: function (data) {

                    
                    let imgs = data.split(",");
                    imgs = imgs.filter(e => e); // Remove any empty elements (if they exist).
                    
                    console.log("Found imgs:", imgs);

                    imgs.forEach((img) => {
                        $("#gallery-imgs").append(new ImgView({model: new Backbone.Model({
                            url: Backbone.history.getFragment() + img,
                            name: img
                        })}).$el);
                    });
                }
            });
        }
    }

    return class Page {
        constructor(url) {

            let model = new PageModel({url: url});
            let view = new PageView({model: model});
        }
    }
});
